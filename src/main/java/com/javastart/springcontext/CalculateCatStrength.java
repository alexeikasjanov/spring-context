package com.javastart.springcontext;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface CalculateCatStrength { // my annotation
    int minStrength();
    int maxStrength();
}
